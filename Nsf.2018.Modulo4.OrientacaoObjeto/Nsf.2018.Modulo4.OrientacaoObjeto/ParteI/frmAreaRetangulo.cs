﻿using Nsf._2018.Modulo4.OrientacaoObjeto.ParteI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo4.OrientacaoObjeto
{
    public partial class frmAreaRetangulo : Form
    {
        public frmAreaRetangulo()
        {
            InitializeComponent();
        }

       
        

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            decimal alturaretangulo, baseretangulo;

            alturaretangulo = decimal.Parse(txtaltura.Text);
            baseretangulo = decimal.Parse(txtbase.Text);

            CalculoAreasGeometricas calculo = new CalculoAreasGeometricas();
            decimal area = calculo.CalcularAreaRetangulo(baseretangulo, alturaretangulo);

            lblresposta.Text = area.ToString();
        }



    }
}
