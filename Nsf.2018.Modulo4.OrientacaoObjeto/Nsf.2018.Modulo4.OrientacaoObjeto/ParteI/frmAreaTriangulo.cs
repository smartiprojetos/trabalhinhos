﻿using Nsf._2018.Modulo4.OrientacaoObjeto.ParteI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo4.OrientacaoObjeto
{
    public partial class frmAreaTriangulo : Form
    {
        public frmAreaTriangulo()
        {
            InitializeComponent();
        }

        

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            decimal base1, altura1,total;
            CalculoAreasGeometricas calculo = new CalculoAreasGeometricas();
            base1 = decimal.Parse(txtbase.Text);
            altura1 = decimal.Parse(txtaltura.Text);
            total = calculo.CalcularAreaTriangulo(base1, altura1);
            lblresposta.Text = total.ToString();
        }


        
        
       
    }
}
