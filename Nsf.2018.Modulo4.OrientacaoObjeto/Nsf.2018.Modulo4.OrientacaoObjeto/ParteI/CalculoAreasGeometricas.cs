﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo4.OrientacaoObjeto.ParteI
{
    public class CalculoAreasGeometricas
    {
        public decimal CalcularAreaRetangulo (decimal baser, decimal alturar)
        {
            decimal total;

            total = baser * alturar;
            return total;
        }

        public decimal CalcularAreaTriangulo (decimal base1, decimal altura1)
        {
            decimal multiplicacao, total;

            multiplicacao = base1 * altura1;
            total = multiplicacao / 2;
            return total;
            

        }
    }
}
